#!/bin/bash

echo 'Enable Multilib repo'
sed -i '93 s/#//' /etc/pacman.conf
sed -i '94 s/#//' /etc/pacman.conf

echo 'Enable Andontie-AUR'
pacman-key --recv-key B545E9B7CD906FE3
pacman-key --lsign-key B545E9B7CD906FE3

if ! grep -q andontie-aur /etc/pacman.conf
then
    echo '[andontie-aur]' >> /etc/pacman.conf
    echo 'Server = https://aur.andontie.net/$arch' >> /etc/pacman.conf
    echo '' >> /etc/pacman.conf 
fi

echo 'Enable Chaotic-AUR'
pacman-key --recv-key 3056513887B78AEB --keyserver keyserver.ubuntu.com
pacman-key --lsign-key 3056513887B78AEB
pacman -U --noconfirm 'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-keyring.pkg.tar.zst' 'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-mirrorlist.pkg.tar.zst'

if ! grep -q chaotic-aur /etc/pacman.conf
then 
    echo '[chaotic-aur]' >> /etc/pacman.conf
    echo 'Include = /etc/pacman.d/chaotic-mirrorlist' >> /etc/pacman.conf
    echo ' ' >> /etc/pacman.conf
fi

echo 'Update Pacman Repos'
pacman -Syy

echo 'Install Apps'

echo 'Install Flatpak'
pacman -S --noconfirm flatpak

# echo 'Install Snapd'
# pacman -S --noconfirm snapd
# systemctl enable --now snapd.socket
# ln -s /var/lib/snapd/snap /snap # Enable Classic Snapd

echo 'Install CLI Tools'
pacman -S --noconfirm paru bat exa dust starship fish zsh bash-completion

echo 'Install Virtualization Apps'
pacman -S --noconfirm virtualbox virt-manager qemu vde2 bridge-utils
systemctl enable --now libvirtd.service

echo 'Install Internet Apps'
pacman -S --noconfirm firefox vivaldi ungoogled-chromium torbrowser-launcher uget aria2 qbittorrent dropbox megasync zoom

echo 'Install Graphics Apps'
pacman -S --noconfirm flameshot spectacle gwenview qt5-imageformats gimp skanlite imagemagick kcolorchooser
flatpak install flathub com.github._4lex4.ScanTailor-Advanced

echo 'Install Multimedia Apps'
pacman -S --noconfirm audacious audacious-plugins gpodder mpv vlc peek youtube-dl

echo 'Install Office Apps'
pacman -S --noconfirm libreoffice-fresh libreoffice-fresh-ro okular ebook-tools foliate calibre typora pandoc

echo 'Install Programming Apps'
pacman -S --noconfirm android-studio genymotion intellij-idea-community-edition pycharm-community-edition vscodium-bin
flatpak install flathub rest.insomnia.Insomnia

echo 'Install Tools Apps'
pacman -S --noconfirm octopi bleachbit grub-customizer gparted thunar gnome-disk-utility bitwarden catfish czkawka-git alacritty filelight kvantum-qt5
