#!/bin/bash

echo 'Setting TimeZone'
ln -sf /usr/share/zoneinfo/Europe/Bucharest /etc/localtime
hwclock --systohc

echo 'Setting Localization'
## Generate the locales
sed -i '177 s/#en_US.UTF-8/en_US.UTF-8/' /etc/locale.gen # English
sed -i '400 s/#ro_RO.UTF-8/ro_RO.UTF-8/' /etc/locale.gen # Romanian
locale-gen

## Set the LANG variable
echo "LANG=en_US.UTF-8" > /etc/locale.conf # ro_RO.UTF-8 for Romanian

## Set the keyboard layout
## echo "KEYMAP=###" >> /etc/vconsole.conf

echo 'Setting Network'
## Create the hostname file
echo "petcu-pc" > /etc/hostname

## Add matching entries to hosts: 
echo "127.0.0.1 localhost" > /etc/hosts
echo "::1       localhost" >> /etc/hosts
echo "127.0.1.1 petcu-pc.localdomain petcu-pc" >> /etc/hosts

echo 'Change Root Password'
echo root:password | chpasswd

echo 'Enable ParallelDownloads and increase the default value'
sed -i '37 s/#//' /etc/pacman.conf
sed -i '37 s/5/10/' /etc/pacman.conf

echo 'Update Pacman Mirrors'
pacman -Syy --noconfirm reflector
reflector -c Romania -a 10 --sort rate --save /etc/pacman.d/mirrorlist
pacman -Syy

echo 'Install Base Services'
pacman -S --noconfirm grub efibootmgr os-prober networkmanager network-manager-applet wpa_supplicant dialog avahi inetutils dnsutils bluez bluez-utils cups mtools dosfstools xdg-utils xdg-user-dirs gvfs gvfs-smb nfs-utils ntfs-3g base-devel linux-lts-headers wget curl virt-manager qemu qemu-arch-extra pipewire pipewire-alsa pipewire-pulse alsa-utils openssh acpi acpi_call edk2-ovmf bridge-utils

echo 'Install GRUB'
grub-install --target=x86_64-efi --efi-directory=/boot/efi/ --bootloader-id=GRUB

if ! grep -q GRUB_DISABLE_OS_PROBER /etc/default/grub
then 
    echo 'GRUB_DISABLE_OS_PROBER=false' >> /etc/default/grub
fi
grub-mkconfig -o /boot/grub/grub.cfg

# Enable Services
systemctl enable NetworkManager
systemctl enable bluetooth
systemctl enable cups.service
systemctl enable sshd
# systemctl enable tlp
systemctl enable fstrim.timer
systemctl enable libvirtd
systemctl enable avahi-daemon

echo 'Adding User'
useradd -m petcu
echo petcu:password | chpasswd
usermod -aG libvirt petcu

echo 'Enable Sudo'
echo "petcu ALL=(ALL) ALL" > /etc/sudoers.d/petcu
