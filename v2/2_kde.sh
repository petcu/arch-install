#!/bin/bash

echo 'Install Base Plasma 5'
pacman -S --noconfirm xorg-server sddm sddm-kcm plasma-desktop plasma-nm plasma-pa plasma-browser-integration plasma-firewall plasma-disks kinfocenter breeze-gtk kde-gtk-config kdeplasma-addons ark konsole dolphin dolphin-plugins kate bluedevil pavucontrol-qt ksysguard powerdevil kscreen kmenuedit kcalc kglobalaccel cpupower khotkeys print-manager system-config-printer kdeconnect

echo 'Enable SDDM Service'
systemctl enable sddm

# Package Description
# -----
# xorg-server - Xorg X server
# sddm - QML based X11 and Wayland display manager
# sddm-kcm - KDE Config Module for SDDM
# plasma-desktop - KDE Plasma Desktop
# plasma-nm - Plasma applet for managing network connections
# plasma-pa - Plasma applet for audio volume
# plasma-browser-integration - Integrate browsers with Plasma
# plasma-firewall - Control Panel for your system firewall
# plasma-disks - Monitors S.M.A.R.T. capable devices
# kinfocenter - A utility that provides information about a system
# breeze-gtk - Breeze widget theme for GTK 2 and 3
# kde-gtk-config - GTK2 and GTK3 Configurator for KDE
# kdeplasma-addons - All kind of addons for Plasma
# ark - Archiving Tool
# konsole - KDE terminal emulator
# dolphin - KDE File Manager
# dolphin-plugins - Extra Dolphin plugins
# kate - Advanced Text Editor
# bluedevil - Integrate the Bluetooth technology within KDE
# pavucontrol-qt - A Pulseaudio mixer in Qt
# ksysguard - Track and control the processes running in system
# powerdevil - Manages the power consumption settings of a Plasma
# kscreen - KDE screen management software
# kmenuedit - KDE menu editor
# kcalc - Scientific Calculator
# kglobalaccel - Add support for global workspace shortcuts
# cpupower - Linux kernel tool to examine and tune power saving
# khotkeys - KHotKeys
# print-manager - A tool for managing print jobs and printers
# system-config-printer - A CUPS printer configuration tool
# kdeconnect - Adds communication between KDE and your smartphone
