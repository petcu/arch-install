#!/bin/bash

echo 'Enable Multilib repo'
sed -i '93 s/#//' /etc/pacman.conf
sed -i '94 s/#//' /etc/pacman.conf

echo 'Enable Chaotic-AUR'
pacman-key --recv-key 3056513887B78AEB --keyserver keyserver.ubuntu.com
pacman-key --lsign-key 3056513887B78AEB
pacman -U --noconfirm 'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-keyring.pkg.tar.zst' 'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-mirrorlist.pkg.tar.zst'

if ! grep -q chaotic-aur /etc/pacman.conf
then 
    echo '[chaotic-aur]' >> /etc/pacman.conf
    echo 'Include = /etc/pacman.d/chaotic-mirrorlist' >> /etc/pacman.conf
    echo ' ' >> /etc/pacman.conf
fi

echo 'Update Pacman Repos'
pacman -Syy

echo 'Install Base Plasma 5'
pacman -S --noconfirm xorg plasma-desktop plasma-nm plasma-pa sddm sddm-kcm cpupower pavucontrol-qt dolphin dolphin-plugins konsole kate kscreen powerdevil ark p7zip unrar bluedevil xf86-input-synaptics plasma-systemmonitor kcalc kde-gtk-config breeze-gtk kdeplasma-addons kinfocenter kmenuedit kglobalaccel

echo 'Enable SDDM Service'
systemctl enable sddm

echo 'Install Apps'
echo 'Install Flatpak'
pacman -S --noconfirm flatpak

echo 'Install Internet Apps'
pacman -S --noconfirm element-desktop ungoogled-chromium firefox uget aria2 qbittorrent megasync dropbox zoom torbrowser-launcher kdeconnect plasma-browser-integration

echo 'Install Graphics Apps'
pacman -S --noconfirm flameshot gwenview gimp skanlite imagemagick spectacle kcolorchooser qt5-imageformats

echo 'Install Multimedia Apps'
pacman -S --noconfirm audacious audacious-plugins gpodder mpv youtube-dl 

echo 'Install Office Apps'
pacman -S --noconfirm libreoffice-fresh libreoffice-fresh-ro typora pandoc calibre foliate system-config-printer print-manager okular

echo 'Install Programming Apps'
pacman -S --noconfirm code bash-completion

echo 'Install Tools Apps'
pacman -S --noconfirm octopi bleachbit grub-customizer gparted thunar gnome-disk-utility bitwarden catfish czkawka-git alacritty-git filelight kvantum-qt5
